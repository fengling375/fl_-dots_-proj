using Component;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;

public class StartBehaviour : MonoBehaviour
{
    [SerializeField] public int count = 100;
    [SerializeField] public float noiseRange = 20f;
    [SerializeField] public float noiseValue = 2f;
    [SerializeField] public float speed = 4f;
    [SerializeField] public float maxHeight = 1f;
    [SerializeField] private Mesh unitMesh;
    [SerializeField] private Material unitMaterial;

    private EntityManager _entityManager;

    void Start()
    {
        _entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        var archetype = _entityManager.CreateArchetype(
            typeof(Rotation),
            typeof(Translation),
            typeof(HeightComponent),
            typeof(SpeedComponent),
            typeof(RenderBounds),
            typeof(LocalToWorld),
            typeof(RenderMesh)
        );
        for (int i = 0; i < count; i++)
        {
            for (int j = 0; j < count; j++)
            {
                CreateEntity(archetype, new Vector3(i * 2, 0, j * 2));
            }
        }
    }

    void CreateEntity(EntityArchetype archetype, Vector3 pos)
    {
        float2 float2 = new float2(pos.x / noiseRange, pos.z / noiseRange);
        float f = noise.snoise(float2) / noiseValue;
        pos.y = f;
        Entity entity = _entityManager.CreateEntity(archetype);
        _entityManager.AddSharedComponentData(entity, new RenderMesh
        {
            mesh = unitMesh,
            material = unitMaterial,
        });
        _entityManager.AddComponentData(entity, new Translation()
        {
            Value = pos
        });
        _entityManager.AddComponentData(entity, new HeightComponent()
        {
            InitiateHeight = f,
            MaxHeight = maxHeight,
        });
        _entityManager.AddComponentData(entity, new SpeedComponent()
        {
            Speed = speed
        });
    }
}