using Component;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;

public class RotationSystem : SystemBase
{
    protected override void OnUpdate()
    {
        Entities.ForEach((ref Translation translation, in Rotation rotation, in HeightComponent heightComponent,
            in SpeedComponent speedComponent) =>
        {
            var elapsedTime = Time.ElapsedTime;
            translation.Value.y =
                (float) math.sin((elapsedTime + heightComponent.InitiateHeight) * speedComponent.Speed) *
                heightComponent.MaxHeight;
        }).WithoutBurst().Run();
        // }).Schedule();
    }
}