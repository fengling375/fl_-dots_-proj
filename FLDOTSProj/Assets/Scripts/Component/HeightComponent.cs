﻿using Unity.Entities;

namespace Component
{
    public class HeightComponent : IComponentData
    {
        public float InitiateHeight;
        public float MaxHeight;
    }
}