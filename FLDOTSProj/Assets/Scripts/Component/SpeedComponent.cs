﻿using Unity.Entities;

namespace Component
{
    public class SpeedComponent : IComponentData
    {
        public float Speed;
    }
}